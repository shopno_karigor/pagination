<html>
    <head>
        <title>Pagination</title>
    </head>

    <?php
    include("vendor/autoload.php");
    use App\Product;
    $obj = new Product();

    $numberOfItemPerPage = 2;
    $totalRow = $obj->getRowNum();
    $totalPage = ceil($totalRow/$numberOfItemPerPage);

    if(!empty($_GET['page'])){
        $currentPage= $_GET['page']-1;
    }
    else{
        $currentPage= 0;
    }

    $offset = $numberOfItemPerPage * $currentPage;
    $b=$obj->index($numberOfItemPerPage,$offset);
    ?>

    <body>
        <h1>
            Product Search
        </h1>

        <form method="post" action="views/search.php">
            <div style="overflow: hidden">
                <input type="text" name="search_key" autofocus="autofocus">
                <input type="submit" value="Search" >
            </div>
        </form>


        <h1>
            Product Details
        </h1>


        <?php

        foreach($b as $item){
            echo '<div style="overflow: hidden">';
                echo'<img src="assets/images/'.$item['product_img'].'" style="float: left;width:100px;height:100px;"/>';
                echo'<span style="padding: 20px">'.$item['product_name'].'</span>';
                echo'<span>'.$item['product_price'].'</span>';
            echo'</div>';
        }

        echo '<div style="overflow: hidden">';
            for($i=1;$i<=$totalPage;$i++){
                echo '<a href="index.php?page='.$i.'" style="padding:10px;">'.$i.'</a>';
            }
        echo'</div>';

        ?>



    </body>
</html>